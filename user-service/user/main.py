from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from user.config import settings
from user.routers import users

app = FastAPI()

origins = [
    settings.CLIENT_ORIGIN,
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


app.include_router(users.router, tags=['Users'], prefix='/users')
