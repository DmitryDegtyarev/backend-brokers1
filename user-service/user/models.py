import uuid
from .database import Base
from sqlalchemy import Column, ForeignKey, String, Integer
from sqlalchemy.dialects.postgresql import UUID
from sqlalchemy.orm import relationship


class User(Base):
    __tablename__ = 'users'
    id = Column(UUID(as_uuid=True), primary_key=True, nullable=False,
                default=uuid.uuid4)
    name = Column(String, nullable=False, unique=True)
    email = Column(String, unique=True, nullable=False)
    device = Column(String, nullable=True)


class Member(Base):
    __tablename__ = 'members'
    id = Column(UUID(as_uuid=True), primary_key=True, nullable=False,
                default=uuid.uuid4)
    project_id = Column(Integer, nullable=True)
    role_id = Column(UUID(as_uuid=True), ForeignKey(
        'roles.id', ondelete='CASCADE'), nullable=True)
    user_id = Column(UUID(as_uuid=True), ForeignKey(
        'users.id', ondelete='CASCADE'), nullable=True)
    role = relationship('Role')
    user = relationship('User')


class Role(Base):
    __tablename__ = 'roles'
    id = Column(UUID(as_uuid=True), primary_key=True, nullable=False,
                default=uuid.uuid4)
    role_name = Column(String, nullable=False, unique=True)
