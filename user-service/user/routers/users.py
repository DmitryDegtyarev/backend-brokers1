from .. import models
from sqlalchemy.orm import Session
from fastapi import Depends, HTTPException, status, APIRouter, Response
from ..database import get_db

# [...] import
router = APIRouter()

# [...] Get All Posts


@router.get('/')
def get_users(db: Session = Depends(get_db), limit: int = 10, page: int = 1, search: str = ''):
    users = db.query(models.User).all()
    return {'status': 'success', 'results': len(users), 'posts': users}


@router.post('/', status_code=status.HTTP_201_CREATED)
def create_user(user, db: Session = Depends(get_db)):
    new_user = models.User(**user.dict())
    db.add(new_user)
    db.commit()
    db.refresh(new_user)
    return new_user


@router.put('/{id}')
def update_user(id: str, user, db: Session = Depends(get_db)):
    user_query = db.query(models.User).filter(models.User.id == id)
    updated_user = user_query.first()

    if not updated_user:
        raise HTTPException(status_code=status.HTTP_200_OK,
                            detail=f'No user with this id: {id} found')

    user_query.update(user.dict(exclude_unset=True), synchronize_session=False)
    db.commit()
    return updated_user


@router.get('/{id}')
def get_user(id: str, db: Session = Depends(get_db)):
    user = db.query(models.User).filter(models.User.id == id).first()
    if not user:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail=f"No user with this id: {id} found")
    return user


@router.delete('/{id}')
def delete_user(id: str, db: Session = Depends(get_db)):
    user_query = db.query(models.User).filter(models.User.id == id)
    user = user_query.first()
    if not user:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail=f'No user with this id: {id} found')

    user_query.delete(synchronize_session=False)
    db.commit()
    return Response(status_code=status.HTTP_204_NO_CONTENT)
