import uuid
from .database import Base
from sqlalchemy import TIMESTAMP, Column, ForeignKey, String, Integer
from sqlalchemy.dialects.postgresql import UUID
from sqlalchemy.orm import relationship

class Member(Base):
    __tablename__ = 'members'
    id = Column(UUID(as_uuid=True), primary_key=True, nullable=False,
                default=uuid.uuid4)
    project_id = Column(Integer, nullable=True)
    role_id = Column(UUID(as_uuid=True), ForeignKey(
        'roles.id', ondelete='CASCADE'), nullable=True)
    user_id = Column(UUID(as_uuid=True), ForeignKey(
        'users.id', ondelete='CASCADE'), nullable=True)
    role = relationship('Role')
    user = relationship('User')