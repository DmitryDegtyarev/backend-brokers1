from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from member.config import settings
from member.routers import members
import uvicorn

app = FastAPI()

origins = [
    settings.CLIENT_ORIGIN,
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


app.include_router(members.router, tags=['Member'], prefix='/members')


if __name__ == "__main__":
    uvicorn.run("member.main:app", host="0.0.0.0", port=8001, reload=True)