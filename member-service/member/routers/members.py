from datetime import datetime
import uuid
from .. import  models
from sqlalchemy.orm import Session
from fastapi import Depends, HTTPException, status, APIRouter, Response
from ..database import get_db

# [...] import
router = APIRouter()

# [...] Get All Posts


@router.get('/')
def get_users(db: Session = Depends(get_db), limit: int = 10, page: int = 1, search: str = ''):
    skip = (page - 1) * limit

    users = db.query(models.Member).all()
    return {'status': 'success', 'results': len(users), 'posts': users}

@router.post('/', status_code=status.HTTP_201_CREATED)
def create_user(user, db: Session = Depends(get_db)):
    new_member = models.Member(**user.dict())
    db.add(new_member)
    db.commit()
    db.refresh(new_member)
    return new_member

@router.put('/{id}')
def update_user(id: str, member, db: Session = Depends(get_db)):
    member_query = db.query(models.Member).filter(models.Member.id == id)
    updated_member = member_query.first()

    if not updated_member:
        raise HTTPException(status_code=status.HTTP_200_OK,
                            detail=f'No member with this id: {id} found')
    
    member_query.update(member.dict(exclude_unset=True), synchronize_session=False)
    db.commit()
    return updated_member

@router.get('/{id}')
def get_user(id: str, db: Session = Depends(get_db)):
    member = db.query(models.Member).filter(models.Member.id == id).first()
    if not member:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail=f"No member with this id: {id} found")
    return member

@router.delete('/{id}')
def delete_user(id: str, db: Session = Depends(get_db)):
    member_query = db.query(models.Member).filter(models.Member.id == id)
    member = member_query.first()
    if not member:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail=f'No member with this id: {id} found')

    member_query.delete(synchronize_session=False)
    db.commit()
    return Response(status_code=status.HTTP_204_NO_CONTENT)

